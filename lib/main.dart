import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:ui';
import 'package:get/get.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xFFF6F6F6),
      statusBarIconBrightness: Brightness.dark,
    ));
    return MaterialApp(debugShowCheckedModeBanner: false, home: ProfilWA());
  }
}

class ChatWa extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(children: [
        Container(
          margin: const EdgeInsets.only(top: 20),
          padding: const EdgeInsets.only(left: 16, right: 16),
          color: Color(0xFFF6F6F6),
          width: Get.width,
          height: 44,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Edit",
                style: TextStyle(fontSize: 16, color: Color(0xFF0074AFF)),
              ),
              Text(
                "Chats",
                style: TextStyle(fontSize: 16, color: Color(0xFF0074AFF)),
              ),
              Image.asset('assets/edit_icon.png')
            ],
          ),
        ),
        Container(
          padding: const EdgeInsets.only(left: 16, right: 16),
          width: Get.width,
          height: 44,
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text(
              "Broadcast List",
              style: TextStyle(fontSize: 16, color: Color(0xFF0074AFF)),
            ),
            Text(
              "New Group",
              style: TextStyle(fontSize: 16, color: Color(0xFF0074AFF)),
            ),
          ]),
        ),
        Container(
          padding: const EdgeInsets.only(left: 16, right: 16),
          width: Get.width,
          child: ListView(
            padding: EdgeInsets.only(top: 20),
            shrinkWrap: true,
            children: [
              ListChat(),
              ListChat(),
              ListChat(),
              ListChat(),
              ListChat(),
              ListChat(),
              ListChat(),
            ],
          ),
        )
      ]),
    );
  }
}

class ListChat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(bottom: 20),
        child: Row(
          children: [
            Expanded(
              flex: 2,
              child: Container(
                margin: EdgeInsets.only(right: 12),
                child: Image.asset(
                  'assets/user_1.png',
                  width: 52,
                  height: 52,
                ),
              ),
            ),
            Expanded(
              child: Container(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: EdgeInsets.only(bottom: 4),
                          child: Text(
                            "Muhammad Mahrus Zain",
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                        ),
                        Text(
                          "21/4/21",
                          style: TextStyle(fontSize: 14, color: Colors.grey),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Image.asset("assets/read.png"),
                        Text(
                          "Bebs jangan lupa buka puasa",
                          style: TextStyle(fontSize: 14, color: Colors.grey),
                        ),
                      ],
                    ),
                    Divider(
                      color: Colors.grey,
                      thickness: 1,
                    )
                  ],
                ),
              ),
              flex: 8,
            ),
            Expanded(
              child: Container(
                child: Image.asset("assets/arrow_right.png"),
              ),
              flex: 1,
            ),
          ],
        ));
  }
}

class ProfilWA extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: ListView(
          children: [
            Column(
              children: [
                Container(
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.all(16),
                        color: Color(0xFFF6F6F6),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(right: 5),
                                  child: Image.asset("assets/back.png"),
                                ),
                                Text(
                                  "Istri 2",
                                  style: TextStyle(
                                      fontSize: 16, color: Color(0xFF0074AFF)),
                                ),
                              ],
                            ),
                            Text(
                              "Contact Info",
                              style: TextStyle(
                                fontSize: 16,
                              ),
                            ),
                            Text(
                              "Edit",
                              style: TextStyle(
                                  fontSize: 16, color: Color(0xFF0074AFF)),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Image.asset("assets/fp.png"),
                      Container(
                        padding: EdgeInsets.all(16),
                        child: Row(
                          children: [
                            Expanded(
                                flex: 1,
                                child: Container(
                                    child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Istri 2",
                                      style: TextStyle(
                                        fontSize: 18,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 4,
                                    ),
                                    Text(
                                      "+62 812 786768762",
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.grey),
                                    ),
                                  ],
                                ))),
                            Expanded(
                                flex: 1,
                                child: Container(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Container(
                                        width: 36,
                                        height: 36,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(16),
                                          color: Color(0xFFEDEDFF),
                                        ),
                                        child: Image.asset("assets/chat.png"),
                                      ),
                                      Container(
                                        width: 36,
                                        height: 36,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(16),
                                          color: Color(0xFFEDEDFF),
                                        ),
                                        child: Image.asset(
                                            "assets/video_call.png"),
                                      ),
                                      Container(
                                        width: 36,
                                        height: 36,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(16),
                                          color: Color(0xFFEDEDFF),
                                        ),
                                        child: Image.asset("assets/call.png"),
                                      )
                                    ],
                                  ),
                                )),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.grey,
                  thickness: 0.6,
                ),
                Container(
                  padding: EdgeInsets.only(left: 16, right: 16),
                  child: Row(
                    children: [
                      Expanded(
                          flex: 1,
                          child: Container(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Design adds value faster, than it adds cost",
                                style: TextStyle(
                                  fontSize: 14,
                                ),
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Text(
                                "Dec 18, 2018",
                                style:
                                    TextStyle(fontSize: 12, color: Colors.grey),
                              ),
                            ],
                          ))),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 4,
                  ),
                  color: Color(0xFFF2F2F2),
                  width: Get.width,
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: 10),
                                width: 30,
                                height: 30,
                                decoration: BoxDecoration(
                                    color: Color(0xFF3396FD),
                                    borderRadius: BorderRadius.circular(8)),
                                child: Image.asset("assets/media.png"),
                              ),
                              Container(
                                child: Text(
                                  "Media, Links, and Docs",
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: 4),
                                child: Text(
                                  "12",
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.grey),
                                ),
                              ),
                              Image.asset("assets/arrow_right.png")
                            ],
                          )
                        ],
                      ),
                      Divider(
                        color: Colors.grey,
                        thickness: 0.2,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: 10),
                                width: 30,
                                height: 30,
                                decoration: BoxDecoration(
                                    color: Color(0xFFFBB500),
                                    borderRadius: BorderRadius.circular(8)),
                                child: Image.asset("assets/star.png"),
                              ),
                              Container(
                                child: Text(
                                  "Starred Messages",
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: 4),
                                child: Text(
                                  "None",
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.grey),
                                ),
                              ),
                              Image.asset("assets/arrow_right.png")
                            ],
                          )
                        ],
                      ),
                      Divider(
                        color: Colors.grey,
                        thickness: 0.2,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        ));
  }
}
